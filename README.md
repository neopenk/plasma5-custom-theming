# plasma5-custom-theming

These are some of my custom Plasma5 themes. Currently I have on this repository:

* A kvantum theme, which is a slightly modified version of the Layan-Dark Kvantum theme by Vince Liuice;

* An aurorae theme based on WhiteSur (also by Vince Liuice);

* A custom colorscheme for Plasma. 

* A GTK theme built using Themix and based on Oomox.

* A btop theme

All of these were modified in order to follow the colors from the Doom One colorscheme, which I also use in Emacs and terminal emulator.

Also included (not modified): 

* Konsole Doom One colorscheme

* WhiteSur + Layan icons mix-and-match 

TODO: custom colors in the Plasma style. Currently I just use WhiteSur because IMO that's the cleanest-looking style, with the Layan icons because I don't want no apples in my GNU/Linux desktop.

TODO: find a way to theme Element/SchildiChat, or find a Matrix client that I can actually theme
